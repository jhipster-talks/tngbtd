import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IBorrow } from 'app/shared/model/borrow.model';

@Component({
  selector: 'jhi-borrow-detail',
  templateUrl: './borrow-detail.component.html',
})
export class BorrowDetailComponent implements OnInit {
  borrow: IBorrow | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ borrow }) => (this.borrow = borrow));
  }

  previousState(): void {
    window.history.back();
  }
}
