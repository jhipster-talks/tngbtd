import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IBorrow } from 'app/shared/model/borrow.model';

type EntityResponseType = HttpResponse<IBorrow>;
type EntityArrayResponseType = HttpResponse<IBorrow[]>;

@Injectable({ providedIn: 'root' })
export class BorrowService {
  public resourceUrl = SERVER_API_URL + 'api/borrows';

  constructor(protected http: HttpClient) {}

  create(borrow: IBorrow): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(borrow);
    return this.http
      .post<IBorrow>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(borrow: IBorrow): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(borrow);
    return this.http
      .put<IBorrow>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IBorrow>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IBorrow[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(borrow: IBorrow): IBorrow {
    const copy: IBorrow = Object.assign({}, borrow, {
      borrowedAt: borrow.borrowedAt && borrow.borrowedAt.isValid() ? borrow.borrowedAt.format(DATE_FORMAT) : undefined,
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.borrowedAt = res.body.borrowedAt ? moment(res.body.borrowedAt) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((borrow: IBorrow) => {
        borrow.borrowedAt = borrow.borrowedAt ? moment(borrow.borrowedAt) : undefined;
      });
    }
    return res;
  }
}
