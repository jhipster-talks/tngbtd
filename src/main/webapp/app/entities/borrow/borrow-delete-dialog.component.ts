import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IBorrow } from 'app/shared/model/borrow.model';
import { BorrowService } from './borrow.service';

@Component({
  templateUrl: './borrow-delete-dialog.component.html',
})
export class BorrowDeleteDialogComponent {
  borrow?: IBorrow;

  constructor(protected borrowService: BorrowService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.borrowService.delete(id).subscribe(() => {
      this.eventManager.broadcast('borrowListModification');
      this.activeModal.close();
    });
  }
}
