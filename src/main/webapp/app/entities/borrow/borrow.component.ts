import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IBorrow } from 'app/shared/model/borrow.model';
import { BorrowService } from './borrow.service';
import { BorrowDeleteDialogComponent } from './borrow-delete-dialog.component';

@Component({
  selector: 'jhi-borrow',
  templateUrl: './borrow.component.html',
})
export class BorrowComponent implements OnInit, OnDestroy {
  borrows?: IBorrow[];
  eventSubscriber?: Subscription;

  constructor(protected borrowService: BorrowService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.borrowService.query().subscribe((res: HttpResponse<IBorrow[]>) => (this.borrows = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInBorrows();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IBorrow): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInBorrows(): void {
    this.eventSubscriber = this.eventManager.subscribe('borrowListModification', () => this.loadAll());
  }

  delete(borrow: IBorrow): void {
    const modalRef = this.modalService.open(BorrowDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.borrow = borrow;
  }
}
