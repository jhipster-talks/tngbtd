import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TngbtdSharedModule } from 'app/shared/shared.module';
import { BorrowComponent } from './borrow.component';
import { BorrowDetailComponent } from './borrow-detail.component';
import { BorrowUpdateComponent } from './borrow-update.component';
import { BorrowDeleteDialogComponent } from './borrow-delete-dialog.component';
import { borrowRoute } from './borrow.route';

@NgModule({
  imports: [TngbtdSharedModule, RouterModule.forChild(borrowRoute)],
  declarations: [BorrowComponent, BorrowDetailComponent, BorrowUpdateComponent, BorrowDeleteDialogComponent],
  entryComponents: [BorrowDeleteDialogComponent],
})
export class TngbtdBorrowModule {}
