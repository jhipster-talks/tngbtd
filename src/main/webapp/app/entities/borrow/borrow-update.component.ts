import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IBorrow, Borrow } from 'app/shared/model/borrow.model';
import { BorrowService } from './borrow.service';
import { IBookCopy } from 'app/shared/model/book-copy.model';
import { BookCopyService } from 'app/entities/book-copy/book-copy.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';

type SelectableEntity = IBookCopy | IUser;

@Component({
  selector: 'jhi-borrow-update',
  templateUrl: './borrow-update.component.html',
})
export class BorrowUpdateComponent implements OnInit {
  isSaving = false;
  bookcopies: IBookCopy[] = [];
  users: IUser[] = [];
  borrowedAtDp: any;

  editForm = this.fb.group({
    id: [],
    borrowedAt: [null, [Validators.required]],
    bookcopy: [],
    user: [],
  });

  constructor(
    protected borrowService: BorrowService,
    protected bookCopyService: BookCopyService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ borrow }) => {
      this.updateForm(borrow);

      this.bookCopyService.query().subscribe((res: HttpResponse<IBookCopy[]>) => (this.bookcopies = res.body || []));

      this.userService.query().subscribe((res: HttpResponse<IUser[]>) => (this.users = res.body || []));
    });
  }

  updateForm(borrow: IBorrow): void {
    this.editForm.patchValue({
      id: borrow.id,
      borrowedAt: borrow.borrowedAt,
      bookcopy: borrow.bookcopy,
      user: borrow.user,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const borrow = this.createFromForm();
    if (borrow.id !== undefined) {
      this.subscribeToSaveResponse(this.borrowService.update(borrow));
    } else {
      this.subscribeToSaveResponse(this.borrowService.create(borrow));
    }
  }

  private createFromForm(): IBorrow {
    return {
      ...new Borrow(),
      id: this.editForm.get(['id'])!.value,
      borrowedAt: this.editForm.get(['borrowedAt'])!.value,
      bookcopy: this.editForm.get(['bookcopy'])!.value,
      user: this.editForm.get(['user'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBorrow>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
