import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IBorrow, Borrow } from 'app/shared/model/borrow.model';
import { BorrowService } from './borrow.service';
import { BorrowComponent } from './borrow.component';
import { BorrowDetailComponent } from './borrow-detail.component';
import { BorrowUpdateComponent } from './borrow-update.component';

@Injectable({ providedIn: 'root' })
export class BorrowResolve implements Resolve<IBorrow> {
  constructor(private service: BorrowService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IBorrow> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((borrow: HttpResponse<Borrow>) => {
          if (borrow.body) {
            return of(borrow.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Borrow());
  }
}

export const borrowRoute: Routes = [
  {
    path: '',
    component: BorrowComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'tngbtdApp.borrow.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: BorrowDetailComponent,
    resolve: {
      borrow: BorrowResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'tngbtdApp.borrow.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: BorrowUpdateComponent,
    resolve: {
      borrow: BorrowResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'tngbtdApp.borrow.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: BorrowUpdateComponent,
    resolve: {
      borrow: BorrowResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'tngbtdApp.borrow.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
