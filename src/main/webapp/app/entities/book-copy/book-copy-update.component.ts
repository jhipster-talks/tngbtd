import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IBookCopy, BookCopy } from 'app/shared/model/book-copy.model';
import { BookCopyService } from './book-copy.service';

@Component({
  selector: 'jhi-book-copy-update',
  templateUrl: './book-copy-update.component.html',
})
export class BookCopyUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    signature: [null, [Validators.required]],
    title: [null, [Validators.required]],
  });

  constructor(protected bookCopyService: BookCopyService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ bookCopy }) => {
      this.updateForm(bookCopy);
    });
  }

  updateForm(bookCopy: IBookCopy): void {
    this.editForm.patchValue({
      id: bookCopy.id,
      signature: bookCopy.signature,
      title: bookCopy.title,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const bookCopy = this.createFromForm();
    if (bookCopy.id !== undefined) {
      this.subscribeToSaveResponse(this.bookCopyService.update(bookCopy));
    } else {
      this.subscribeToSaveResponse(this.bookCopyService.create(bookCopy));
    }
  }

  private createFromForm(): IBookCopy {
    return {
      ...new BookCopy(),
      id: this.editForm.get(['id'])!.value,
      signature: this.editForm.get(['signature'])!.value,
      title: this.editForm.get(['title'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBookCopy>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
