import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IBookCopy } from 'app/shared/model/book-copy.model';
import { BookCopyService } from './book-copy.service';

@Component({
  templateUrl: './book-copy-delete-dialog.component.html',
})
export class BookCopyDeleteDialogComponent {
  bookCopy?: IBookCopy;

  constructor(protected bookCopyService: BookCopyService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.bookCopyService.delete(id).subscribe(() => {
      this.eventManager.broadcast('bookCopyListModification');
      this.activeModal.close();
    });
  }
}
