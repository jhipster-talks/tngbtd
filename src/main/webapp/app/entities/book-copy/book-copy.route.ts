import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IBookCopy, BookCopy } from 'app/shared/model/book-copy.model';
import { BookCopyService } from './book-copy.service';
import { BookCopyComponent } from './book-copy.component';
import { BookCopyDetailComponent } from './book-copy-detail.component';
import { BookCopyUpdateComponent } from './book-copy-update.component';

@Injectable({ providedIn: 'root' })
export class BookCopyResolve implements Resolve<IBookCopy> {
  constructor(private service: BookCopyService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IBookCopy> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((bookCopy: HttpResponse<BookCopy>) => {
          if (bookCopy.body) {
            return of(bookCopy.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new BookCopy());
  }
}

export const bookCopyRoute: Routes = [
  {
    path: '',
    component: BookCopyComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'tngbtdApp.bookCopy.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: BookCopyDetailComponent,
    resolve: {
      bookCopy: BookCopyResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'tngbtdApp.bookCopy.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: BookCopyUpdateComponent,
    resolve: {
      bookCopy: BookCopyResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'tngbtdApp.bookCopy.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: BookCopyUpdateComponent,
    resolve: {
      bookCopy: BookCopyResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'tngbtdApp.bookCopy.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
