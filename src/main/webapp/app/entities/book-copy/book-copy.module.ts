import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TngbtdSharedModule } from 'app/shared/shared.module';
import { BookCopyComponent } from './book-copy.component';
import { BookCopyDetailComponent } from './book-copy-detail.component';
import { BookCopyUpdateComponent } from './book-copy-update.component';
import { BookCopyDeleteDialogComponent } from './book-copy-delete-dialog.component';
import { bookCopyRoute } from './book-copy.route';

@NgModule({
  imports: [TngbtdSharedModule, RouterModule.forChild(bookCopyRoute)],
  declarations: [BookCopyComponent, BookCopyDetailComponent, BookCopyUpdateComponent, BookCopyDeleteDialogComponent],
  entryComponents: [BookCopyDeleteDialogComponent],
})
export class TngbtdBookCopyModule {}
