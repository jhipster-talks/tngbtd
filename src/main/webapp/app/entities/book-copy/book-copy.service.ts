import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IBookCopy } from 'app/shared/model/book-copy.model';

type EntityResponseType = HttpResponse<IBookCopy>;
type EntityArrayResponseType = HttpResponse<IBookCopy[]>;

@Injectable({ providedIn: 'root' })
export class BookCopyService {
  public resourceUrl = SERVER_API_URL + 'api/book-copies';

  constructor(protected http: HttpClient) {}

  create(bookCopy: IBookCopy): Observable<EntityResponseType> {
    return this.http.post<IBookCopy>(this.resourceUrl, bookCopy, { observe: 'response' });
  }

  update(bookCopy: IBookCopy): Observable<EntityResponseType> {
    return this.http.put<IBookCopy>(this.resourceUrl, bookCopy, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IBookCopy>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IBookCopy[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
