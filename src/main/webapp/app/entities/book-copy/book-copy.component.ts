import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IBookCopy } from 'app/shared/model/book-copy.model';
import { BookCopyService } from './book-copy.service';
import { BookCopyDeleteDialogComponent } from './book-copy-delete-dialog.component';

@Component({
  selector: 'jhi-book-copy',
  templateUrl: './book-copy.component.html',
})
export class BookCopyComponent implements OnInit, OnDestroy {
  bookCopies?: IBookCopy[];
  eventSubscriber?: Subscription;

  constructor(protected bookCopyService: BookCopyService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.bookCopyService.query().subscribe((res: HttpResponse<IBookCopy[]>) => (this.bookCopies = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInBookCopies();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IBookCopy): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInBookCopies(): void {
    this.eventSubscriber = this.eventManager.subscribe('bookCopyListModification', () => this.loadAll());
  }

  delete(bookCopy: IBookCopy): void {
    const modalRef = this.modalService.open(BookCopyDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.bookCopy = bookCopy;
  }
}
