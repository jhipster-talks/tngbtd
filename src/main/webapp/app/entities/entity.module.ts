import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'book-copy',
        loadChildren: () => import('./book-copy/book-copy.module').then(m => m.TngbtdBookCopyModule),
      },
      {
        path: 'borrow',
        loadChildren: () => import('./borrow/borrow.module').then(m => m.TngbtdBorrowModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class TngbtdEntityModule {}
