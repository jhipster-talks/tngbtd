import { Moment } from 'moment';
import { IBookCopy } from 'app/shared/model/book-copy.model';
import { IUser } from 'app/core/user/user.model';

export interface IBorrow {
  id?: number;
  borrowedAt?: Moment;
  bookcopy?: IBookCopy;
  user?: IUser;
}

export class Borrow implements IBorrow {
  constructor(public id?: number, public borrowedAt?: Moment, public bookcopy?: IBookCopy, public user?: IUser) {}
}
