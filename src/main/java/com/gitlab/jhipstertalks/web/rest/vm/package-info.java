/**
 * View Models used by Spring MVC REST controllers.
 */
package com.gitlab.jhipstertalks.web.rest.vm;
