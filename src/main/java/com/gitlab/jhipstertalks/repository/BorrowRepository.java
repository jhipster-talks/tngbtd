package com.gitlab.jhipstertalks.repository;

import com.gitlab.jhipstertalks.domain.Borrow;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Borrow entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BorrowRepository extends JpaRepository<Borrow, Long> {

    @Query("select borrow from Borrow borrow where borrow.user.login = ?#{principal.username}")
    List<Borrow> findByUserIsCurrentUser();
}
