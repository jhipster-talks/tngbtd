import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { BorrowComponentsPage, BorrowDeleteDialog, BorrowUpdatePage } from './borrow.page-object';

const expect = chai.expect;

describe('Borrow e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let borrowComponentsPage: BorrowComponentsPage;
  let borrowUpdatePage: BorrowUpdatePage;
  let borrowDeleteDialog: BorrowDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Borrows', async () => {
    await navBarPage.goToEntity('borrow');
    borrowComponentsPage = new BorrowComponentsPage();
    await browser.wait(ec.visibilityOf(borrowComponentsPage.title), 5000);
    expect(await borrowComponentsPage.getTitle()).to.eq('tngbtdApp.borrow.home.title');
    await browser.wait(ec.or(ec.visibilityOf(borrowComponentsPage.entities), ec.visibilityOf(borrowComponentsPage.noResult)), 1000);
  });

  it('should load create Borrow page', async () => {
    await borrowComponentsPage.clickOnCreateButton();
    borrowUpdatePage = new BorrowUpdatePage();
    expect(await borrowUpdatePage.getPageTitle()).to.eq('tngbtdApp.borrow.home.createOrEditLabel');
    await borrowUpdatePage.cancel();
  });

  it('should create and save Borrows', async () => {
    const nbButtonsBeforeCreate = await borrowComponentsPage.countDeleteButtons();

    await borrowComponentsPage.clickOnCreateButton();

    await promise.all([
      borrowUpdatePage.setBorrowedAtInput('2000-12-31'),
      borrowUpdatePage.bookcopySelectLastOption(),
      borrowUpdatePage.userSelectLastOption(),
    ]);

    expect(await borrowUpdatePage.getBorrowedAtInput()).to.eq('2000-12-31', 'Expected borrowedAt value to be equals to 2000-12-31');

    await borrowUpdatePage.save();
    expect(await borrowUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await borrowComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Borrow', async () => {
    const nbButtonsBeforeDelete = await borrowComponentsPage.countDeleteButtons();
    await borrowComponentsPage.clickOnLastDeleteButton();

    borrowDeleteDialog = new BorrowDeleteDialog();
    expect(await borrowDeleteDialog.getDialogTitle()).to.eq('tngbtdApp.borrow.delete.question');
    await borrowDeleteDialog.clickOnConfirmButton();

    expect(await borrowComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
