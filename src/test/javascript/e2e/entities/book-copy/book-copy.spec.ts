import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { BookCopyComponentsPage, BookCopyDeleteDialog, BookCopyUpdatePage } from './book-copy.page-object';

const expect = chai.expect;

describe('BookCopy e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let bookCopyComponentsPage: BookCopyComponentsPage;
  let bookCopyUpdatePage: BookCopyUpdatePage;
  let bookCopyDeleteDialog: BookCopyDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load BookCopies', async () => {
    await navBarPage.goToEntity('book-copy');
    bookCopyComponentsPage = new BookCopyComponentsPage();
    await browser.wait(ec.visibilityOf(bookCopyComponentsPage.title), 5000);
    expect(await bookCopyComponentsPage.getTitle()).to.eq('tngbtdApp.bookCopy.home.title');
    await browser.wait(ec.or(ec.visibilityOf(bookCopyComponentsPage.entities), ec.visibilityOf(bookCopyComponentsPage.noResult)), 1000);
  });

  it('should load create BookCopy page', async () => {
    await bookCopyComponentsPage.clickOnCreateButton();
    bookCopyUpdatePage = new BookCopyUpdatePage();
    expect(await bookCopyUpdatePage.getPageTitle()).to.eq('tngbtdApp.bookCopy.home.createOrEditLabel');
    await bookCopyUpdatePage.cancel();
  });

  it('should create and save BookCopies', async () => {
    const nbButtonsBeforeCreate = await bookCopyComponentsPage.countDeleteButtons();

    await bookCopyComponentsPage.clickOnCreateButton();

    await promise.all([bookCopyUpdatePage.setSignatureInput('signature'), bookCopyUpdatePage.setTitleInput('title')]);

    expect(await bookCopyUpdatePage.getSignatureInput()).to.eq('signature', 'Expected Signature value to be equals to signature');
    expect(await bookCopyUpdatePage.getTitleInput()).to.eq('title', 'Expected Title value to be equals to title');

    await bookCopyUpdatePage.save();
    expect(await bookCopyUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await bookCopyComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last BookCopy', async () => {
    const nbButtonsBeforeDelete = await bookCopyComponentsPage.countDeleteButtons();
    await bookCopyComponentsPage.clickOnLastDeleteButton();

    bookCopyDeleteDialog = new BookCopyDeleteDialog();
    expect(await bookCopyDeleteDialog.getDialogTitle()).to.eq('tngbtdApp.bookCopy.delete.question');
    await bookCopyDeleteDialog.clickOnConfirmButton();

    expect(await bookCopyComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
