import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TngbtdTestModule } from '../../../test.module';
import { BorrowComponent } from 'app/entities/borrow/borrow.component';
import { BorrowService } from 'app/entities/borrow/borrow.service';
import { Borrow } from 'app/shared/model/borrow.model';

describe('Component Tests', () => {
  describe('Borrow Management Component', () => {
    let comp: BorrowComponent;
    let fixture: ComponentFixture<BorrowComponent>;
    let service: BorrowService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TngbtdTestModule],
        declarations: [BorrowComponent],
      })
        .overrideTemplate(BorrowComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BorrowComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BorrowService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Borrow(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.borrows && comp.borrows[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
