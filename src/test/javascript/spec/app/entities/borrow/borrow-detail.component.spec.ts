import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { TngbtdTestModule } from '../../../test.module';
import { BorrowDetailComponent } from 'app/entities/borrow/borrow-detail.component';
import { Borrow } from 'app/shared/model/borrow.model';

describe('Component Tests', () => {
  describe('Borrow Management Detail Component', () => {
    let comp: BorrowDetailComponent;
    let fixture: ComponentFixture<BorrowDetailComponent>;
    const route = ({ data: of({ borrow: new Borrow(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TngbtdTestModule],
        declarations: [BorrowDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(BorrowDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BorrowDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load borrow on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.borrow).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
