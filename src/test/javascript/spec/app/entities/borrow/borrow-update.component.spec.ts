import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { TngbtdTestModule } from '../../../test.module';
import { BorrowUpdateComponent } from 'app/entities/borrow/borrow-update.component';
import { BorrowService } from 'app/entities/borrow/borrow.service';
import { Borrow } from 'app/shared/model/borrow.model';

describe('Component Tests', () => {
  describe('Borrow Management Update Component', () => {
    let comp: BorrowUpdateComponent;
    let fixture: ComponentFixture<BorrowUpdateComponent>;
    let service: BorrowService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TngbtdTestModule],
        declarations: [BorrowUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(BorrowUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BorrowUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BorrowService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Borrow(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Borrow();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
