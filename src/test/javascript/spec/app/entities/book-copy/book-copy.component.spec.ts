import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TngbtdTestModule } from '../../../test.module';
import { BookCopyComponent } from 'app/entities/book-copy/book-copy.component';
import { BookCopyService } from 'app/entities/book-copy/book-copy.service';
import { BookCopy } from 'app/shared/model/book-copy.model';

describe('Component Tests', () => {
  describe('BookCopy Management Component', () => {
    let comp: BookCopyComponent;
    let fixture: ComponentFixture<BookCopyComponent>;
    let service: BookCopyService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TngbtdTestModule],
        declarations: [BookCopyComponent],
      })
        .overrideTemplate(BookCopyComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BookCopyComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BookCopyService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new BookCopy(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.bookCopies && comp.bookCopies[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
