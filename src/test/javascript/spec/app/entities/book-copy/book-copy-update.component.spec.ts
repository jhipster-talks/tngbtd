import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { TngbtdTestModule } from '../../../test.module';
import { BookCopyUpdateComponent } from 'app/entities/book-copy/book-copy-update.component';
import { BookCopyService } from 'app/entities/book-copy/book-copy.service';
import { BookCopy } from 'app/shared/model/book-copy.model';

describe('Component Tests', () => {
  describe('BookCopy Management Update Component', () => {
    let comp: BookCopyUpdateComponent;
    let fixture: ComponentFixture<BookCopyUpdateComponent>;
    let service: BookCopyService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TngbtdTestModule],
        declarations: [BookCopyUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(BookCopyUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BookCopyUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BookCopyService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new BookCopy(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new BookCopy();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
