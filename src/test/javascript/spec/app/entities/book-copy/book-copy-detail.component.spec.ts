import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { TngbtdTestModule } from '../../../test.module';
import { BookCopyDetailComponent } from 'app/entities/book-copy/book-copy-detail.component';
import { BookCopy } from 'app/shared/model/book-copy.model';

describe('Component Tests', () => {
  describe('BookCopy Management Detail Component', () => {
    let comp: BookCopyDetailComponent;
    let fixture: ComponentFixture<BookCopyDetailComponent>;
    const route = ({ data: of({ bookCopy: new BookCopy(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TngbtdTestModule],
        declarations: [BookCopyDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(BookCopyDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BookCopyDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load bookCopy on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.bookCopy).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
