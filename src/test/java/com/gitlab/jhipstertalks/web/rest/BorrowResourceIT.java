package com.gitlab.jhipstertalks.web.rest;

import com.gitlab.jhipstertalks.TngbtdApp;
import com.gitlab.jhipstertalks.domain.Borrow;
import com.gitlab.jhipstertalks.repository.BorrowRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link BorrowResource} REST controller.
 */
@SpringBootTest(classes = TngbtdApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class BorrowResourceIT {

    private static final LocalDate DEFAULT_BORROWED_AT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_BORROWED_AT = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private BorrowRepository borrowRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restBorrowMockMvc;

    private Borrow borrow;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Borrow createEntity(EntityManager em) {
        Borrow borrow = new Borrow()
            .borrowedAt(DEFAULT_BORROWED_AT);
        return borrow;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Borrow createUpdatedEntity(EntityManager em) {
        Borrow borrow = new Borrow()
            .borrowedAt(UPDATED_BORROWED_AT);
        return borrow;
    }

    @BeforeEach
    public void initTest() {
        borrow = createEntity(em);
    }

    @Test
    @Transactional
    public void createBorrow() throws Exception {
        int databaseSizeBeforeCreate = borrowRepository.findAll().size();
        // Create the Borrow
        restBorrowMockMvc.perform(post("/api/borrows")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(borrow)))
            .andExpect(status().isCreated());

        // Validate the Borrow in the database
        List<Borrow> borrowList = borrowRepository.findAll();
        assertThat(borrowList).hasSize(databaseSizeBeforeCreate + 1);
        Borrow testBorrow = borrowList.get(borrowList.size() - 1);
        assertThat(testBorrow.getBorrowedAt()).isEqualTo(DEFAULT_BORROWED_AT);
    }

    @Test
    @Transactional
    public void createBorrowWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = borrowRepository.findAll().size();

        // Create the Borrow with an existing ID
        borrow.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBorrowMockMvc.perform(post("/api/borrows")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(borrow)))
            .andExpect(status().isBadRequest());

        // Validate the Borrow in the database
        List<Borrow> borrowList = borrowRepository.findAll();
        assertThat(borrowList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkBorrowedAtIsRequired() throws Exception {
        int databaseSizeBeforeTest = borrowRepository.findAll().size();
        // set the field null
        borrow.setBorrowedAt(null);

        // Create the Borrow, which fails.


        restBorrowMockMvc.perform(post("/api/borrows")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(borrow)))
            .andExpect(status().isBadRequest());

        List<Borrow> borrowList = borrowRepository.findAll();
        assertThat(borrowList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllBorrows() throws Exception {
        // Initialize the database
        borrowRepository.saveAndFlush(borrow);

        // Get all the borrowList
        restBorrowMockMvc.perform(get("/api/borrows?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(borrow.getId().intValue())))
            .andExpect(jsonPath("$.[*].borrowedAt").value(hasItem(DEFAULT_BORROWED_AT.toString())));
    }
    
    @Test
    @Transactional
    public void getBorrow() throws Exception {
        // Initialize the database
        borrowRepository.saveAndFlush(borrow);

        // Get the borrow
        restBorrowMockMvc.perform(get("/api/borrows/{id}", borrow.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(borrow.getId().intValue()))
            .andExpect(jsonPath("$.borrowedAt").value(DEFAULT_BORROWED_AT.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingBorrow() throws Exception {
        // Get the borrow
        restBorrowMockMvc.perform(get("/api/borrows/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBorrow() throws Exception {
        // Initialize the database
        borrowRepository.saveAndFlush(borrow);

        int databaseSizeBeforeUpdate = borrowRepository.findAll().size();

        // Update the borrow
        Borrow updatedBorrow = borrowRepository.findById(borrow.getId()).get();
        // Disconnect from session so that the updates on updatedBorrow are not directly saved in db
        em.detach(updatedBorrow);
        updatedBorrow
            .borrowedAt(UPDATED_BORROWED_AT);

        restBorrowMockMvc.perform(put("/api/borrows")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedBorrow)))
            .andExpect(status().isOk());

        // Validate the Borrow in the database
        List<Borrow> borrowList = borrowRepository.findAll();
        assertThat(borrowList).hasSize(databaseSizeBeforeUpdate);
        Borrow testBorrow = borrowList.get(borrowList.size() - 1);
        assertThat(testBorrow.getBorrowedAt()).isEqualTo(UPDATED_BORROWED_AT);
    }

    @Test
    @Transactional
    public void updateNonExistingBorrow() throws Exception {
        int databaseSizeBeforeUpdate = borrowRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBorrowMockMvc.perform(put("/api/borrows")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(borrow)))
            .andExpect(status().isBadRequest());

        // Validate the Borrow in the database
        List<Borrow> borrowList = borrowRepository.findAll();
        assertThat(borrowList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteBorrow() throws Exception {
        // Initialize the database
        borrowRepository.saveAndFlush(borrow);

        int databaseSizeBeforeDelete = borrowRepository.findAll().size();

        // Delete the borrow
        restBorrowMockMvc.perform(delete("/api/borrows/{id}", borrow.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Borrow> borrowList = borrowRepository.findAll();
        assertThat(borrowList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
